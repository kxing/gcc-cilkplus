/*
 * kxing: Header file for getting measurement statistics.
 */

#ifndef MEASUREMENTS_H_INCLUDED
#define MEASUREMENTS_H_INCLUDED

#include <cilk/common.h>

typedef struct {
    long long work;
    long long span;
} cilk_statistics;

/*
 * Sample usage:
 *
 * BEGIN_CILK_RECORD_REGION(stats) {
 *     ... = cilk_spawn(...);
 *     cilk_sync;
 * } END_CILK_RECORD_REGION(stats);
 */

#define BEGIN_CILK_RECORD_REGION(s) __cilkrts_begin_record_region(s); do
#define END_CILK_RECORD_REGION(s) while (0); __cilkrts_end_record_region(s)

CILK_ABI(void) __cilkrts_begin_record_region(cilk_statistics *stats);
CILK_ABI(void) __cilkrts_end_record_region(cilk_statistics *stats);

#endif // MEASUREMENTS_H_INCLUDED
