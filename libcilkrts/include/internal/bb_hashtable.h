#ifndef _BB_HASHTABLE_H_
#define _BB_HASHTABLE_H_

typedef int bb_id_t;

void __cilkrts_bb_hashtable_initialize();
void __cilkrts_bb_hashtable_deinitialize();

bb_id_t __cilkrts_bb_hashtable_get_id(void *start, void *end);

void *__cilkrts_bb_hashtable_fetch_start(bb_id_t id);
void *__cilkrts_bb_hashtable_fetch_end(bb_id_t id);

#endif  /* _BB_HASHTABLE_H_ */
