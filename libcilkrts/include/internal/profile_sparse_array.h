#include "internal/profile_array.h"
#include "internal/profile_linked_list.h"

typedef struct __cilkrts_profile_sparse_array __cilkrts_profile_sparse_array;

struct __cilkrts_profile_sparse_array {
    __cilkrts_profile_array *array;
    __cilkrts_profile_linked_list *list;
};

__cilkrts_profile_sparse_array *__cilkrts_create_profile_sparse_array();

void __cilkrts_destroy_profile_sparse_array(
        __cilkrts_profile_sparse_array *spa);

void __cilkrts_profile_sparse_array_update(
        __cilkrts_profile_sparse_array *spa,
        int id,
        long long time);

/* Deletes the input sparse arrays and returns a newly allocated one. */
__cilkrts_profile_sparse_array *__cilkrts_profile_sparse_array_sum(
        __cilkrts_profile_sparse_array *spa1,
        __cilkrts_profile_sparse_array *spa2);

void __cilkrts_profile_sparse_array_finalize(
        __cilkrts_profile_sparse_array *spa);

long long __cilkrts_profile_sparse_array_get(
        __cilkrts_profile_sparse_array *spa,
        int id);

int __cilkrts_profile_sparse_array_get_capacity(
        __cilkrts_profile_sparse_array *spa);
