#ifndef _PROFILE_H_
#define _PROFILE_H_

#include <stdio.h>

#include "internal/bb_hashtable.h"
#include "internal/cc_hashtable.h"
#include "internal/profile_sparse_array.h"

typedef struct __cilkrts_work_span __cilkrts_work_span;
typedef struct __cilkrts_profile __cilkrts_profile;

#ifdef CHECK_FOR_PROFILE_LEAKS
extern long long profiles_allocated;
#endif /* CHECK_FOR_PROFILE_LEAKS */

struct __cilkrts_work_span {
    long long work;
    long long span;
    __cilkrts_work_span *next;
};

__cilkrts_work_span *__cilkrts_create_work_span();

void __cilkrts_destroy_work_span(__cilkrts_work_span *ws);

struct __cilkrts_profile {
    __cilkrts_work_span *ws;

    __cilkrts_profile_sparse_array *work_on_work_cc_spa;
    __cilkrts_profile_sparse_array *span_on_work_cc_spa;
    __cilkrts_profile_sparse_array *work_on_span_cc_spa;
    __cilkrts_profile_sparse_array *span_on_span_cc_spa;

    __cilkrts_profile_sparse_array *work_bb_spa;
    __cilkrts_profile_sparse_array *span_bb_spa;
};

__cilkrts_profile *__cilkrts_create_profile();

void __cilkrts_destroy_profile(__cilkrts_profile *p);

void __cilkrts_update_cc_profile(__cilkrts_profile *p,
                                 cc_id_t id,
                                 long long work,
                                 long long span);

void __cilkrts_update_bb_profile(__cilkrts_profile *p,
                                 bb_id_t id,
                                 long long work,
                                 long long span);

/* Destroys both profiles, and creates a new one. */
__cilkrts_profile *__cilkrts_series_merge_profiles(
        __cilkrts_profile *child_profile,
        __cilkrts_profile *parent_profile);

/* Destroys both profiles, and creates a new one. */
__cilkrts_profile *__cilkrts_parallel_merge_profiles(
        __cilkrts_profile *profile1,
        __cilkrts_profile *profile2);

void __cilkrts_print_cc_profile(__cilkrts_profile *p, FILE *cc_stream);

void __cilkrts_print_bb_profile(__cilkrts_profile *p, FILE *bb_stream);

#endif  /* _PROFILE_H_ */
