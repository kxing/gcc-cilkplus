#ifndef _CC_HASHTABLE_H_
#define _CC_HASHTABLE_H_

typedef int cc_id_t;

void __cilkrts_cc_hashtable_initialize();
void __cilkrts_cc_hashtable_deinitialize();

cc_id_t __cilkrts_cc_hashtable_get_id(void *caller,
                                      void *callee,
                                      int callee_cilk_height);

void *__cilkrts_cc_hashtable_fetch_caller(cc_id_t id);
void *__cilkrts_cc_hashtable_fetch_callee(cc_id_t id);
int __cilkrts_cc_hashtable_fetch_callee_cilk_height(cc_id_t id);

#endif  /* _CC_HASHTABLE_H_ */
