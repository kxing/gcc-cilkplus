#ifndef _PROFILE_ARRAY_H_
#define _PROFILE_ARRAY_H_

typedef struct __cilkrts_profile_array __cilkrts_profile_array;

struct __cilkrts_profile_array {
    long long *times;
    int capacity;
};

__cilkrts_profile_array *__cilkrts_create_profile_array();

void __cilkrts_destroy_profile_array(__cilkrts_profile_array *array);

void __cilkrts_expand_profile_array(__cilkrts_profile_array *array,
                                    int minimum_capacity);

#endif  /* _PROFILE_ARRAY_H_ */
