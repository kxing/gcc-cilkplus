#ifndef _PROFILE_LINKED_LIST_H_
#define _PROFILE_LINKED_LIST_H_

#include "internal/cc_hashtable.h"

typedef struct __cilkrts_profile_linked_list_node
               __cilkrts_profile_linked_list_node;

typedef struct __cilkrts_profile_linked_list
               __cilkrts_profile_linked_list;

struct __cilkrts_profile_linked_list_node {
    long long time;
    /* Either a bb_id_t or cc_id_t, which are both ints. */
    int id;
    __cilkrts_profile_linked_list_node *next;
};

struct __cilkrts_profile_linked_list {
    __cilkrts_profile_linked_list_node *head;
    __cilkrts_profile_linked_list_node *tail;
    int size;
    int largest_id;
};

__cilkrts_profile_linked_list_node *__cilkrts_create_profile_linked_list_node();

void __cilkrts_destroy_profile_linked_list_node(
        __cilkrts_profile_linked_list_node *node);

__cilkrts_profile_linked_list *__cilkrts_create_profile_linked_list();

void __cilkrts_destroy_profile_linked_list(__cilkrts_profile_linked_list *list);

#endif  /* _PROFILE_LINKED_LIST_H_ */
