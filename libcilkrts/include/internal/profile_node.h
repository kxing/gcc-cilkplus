#ifndef _PROFILE_NODE_H_
#define _PROFILE_NODE_H_

#include "profile.h"

typedef struct __cilkrts_profile_node __cilkrts_profile_node;

#ifdef CHECK_FOR_PROFILE_NODE_LEAKS
extern long long profile_nodes_allocated;
#endif /* CHECK_FOR_PROFILE_NODE_LEAKS */

struct __cilkrts_profile_node {
    __cilkrts_profile_node *parent;
    __cilkrts_profile_node *spawned_child;
    __cilkrts_profile_node *continuation_child;
    __cilkrts_profile *contents;
};

__cilkrts_profile_node *__cilkrts_create_profile_node();

void __cilkrts_destroy_profile_node(__cilkrts_profile_node *profile_node);

void __cilkrts_spawn_link_profile_nodes(__cilkrts_profile_node *parent,
                                        __cilkrts_profile_node *child);

void __cilkrts_continuation_link_profile_nodes(__cilkrts_profile_node *parent,
                                               __cilkrts_profile_node *child);

/* Note: we can make this function slightly more efficient, but it's probably
         unnecessary . */
void __cilkrts_unlink_profile_nodes(__cilkrts_profile_node *parent,
                                    __cilkrts_profile_node *child);

void __cilkrts_set_profile_node_contents(__cilkrts_profile_node *profile_node,
                                         __cilkrts_profile *contents);

__cilkrts_profile *__cilkrts_get_profile_node_contents(
        __cilkrts_profile_node *profile_node);

__cilkrts_profile *__cilkrts_remove_profile_node_contents(
        __cilkrts_profile_node *profile_node);

/* Assumes there is a continuation child of the current node. */
void __cilkrts_reduce_profile_node(__cilkrts_profile_node *profile_node);

#endif  /* _PROFILE_NODE_H_ */
