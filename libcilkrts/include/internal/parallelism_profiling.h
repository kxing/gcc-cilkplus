#ifndef PARALLELISM_PROFILING_H
#define PARALLELISM_PROFILING_H

#include <limits.h>

#include "abi.h"
#include "profile_node.h"
#include "runtime/bug.h"

/* (kxing): Reusable code for updating a __cilkrts_stack_frame with elapsed
            time. */
static inline void stack_frame_update_elapsed_time(__cilkrts_stack_frame *sf,
                                                   long long elapsed_time) {
    sf->work += elapsed_time;
    sf->span += elapsed_time;
}

/* (kxing): Reusable code for doing a sync update on a __cilkrts_stack_frame. */
static inline void stack_frame_sync_update(__cilkrts_stack_frame *sf) {
    sf->work += sf->children_work;
    sf->children_work = 0;
    sf->span = (sf->span > sf->children_span) ?
                sf->span : sf->children_span;
    sf->children_span = LLONG_MIN;
}

/* (kxing): Handles work and span updating when the execution flow goes from a
            parent to a child. */
static inline void stack_frame_transfer_parent_to_child(
        __cilkrts_stack_frame *parent,
        __cilkrts_stack_frame *child) {
    child->work = parent->work;
    child->span = parent->span;
    child->children_work = 0;
    child->children_span = LLONG_MIN;

    parent->work = 0;
}

/* (kxing): Handles work and span updating when the execution flow goes from a
            spawn-helper child to a parent. */
static inline void stack_frame_transfer_spawn_helper_child_to_parent(
        __cilkrts_stack_frame *child,
        __cilkrts_stack_frame *parent) {
    parent->children_work += child->work;
    parent->children_span = (parent->children_span > child->span) ?
                             parent->children_span : child->span;
    child->work = 0;
    CILK_ASSERT(child->children_work == 0);
}

/* (kxing): Handles work and span updating when the execution flow goes from a
            non-spawn-helper child to a parent. */
static inline void stack_frame_transfer_non_spawn_helper_child_to_parent(
        __cilkrts_stack_frame *child,
        __cilkrts_stack_frame *parent) {
    parent->work += child->work;
    parent->span = (parent->span > child->span) ?
                    parent->span : child->span;
    child->work = 0;
    CILK_ASSERT(child->children_work == 0);
}

/* (kxing): Handles work and span updating when the execution flow goes from a
            child to a parent. */
static inline void stack_frame_transfer_child_to_parent(
        __cilkrts_stack_frame *child,
        __cilkrts_stack_frame *parent) {
    if (child->is_spawn_helper) {
        stack_frame_transfer_spawn_helper_child_to_parent(child, parent);
    } else {
        stack_frame_transfer_non_spawn_helper_child_to_parent(child, parent);
    }
}

/* (kxing): Handles reducing profile nodes in a stack frame. */
static inline void stack_frame_reduce_profile_nodes(__cilkrts_stack_frame *sf)
{
    /* Squash down the profile nodes until we are where we were at the beginning
       of the function. */
    while (sf->topmost_profile_node != sf->function_profile_node) {
        __cilkrts_profile_node *parent_pn = sf->topmost_profile_node->parent;
        __cilkrts_reduce_profile_node(parent_pn);
        sf->topmost_profile_node = parent_pn;
    }
}


#endif
