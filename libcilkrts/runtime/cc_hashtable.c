#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cilk_malloc.h"
#include "os_mutex.h"
#include "internal/cc_hashtable.h"

static os_mutex *lock;

/* The "entries" are implemented as an array. */
typedef struct cc_entry cc_entry;

struct cc_entry {
    void *caller;
    void *callee;
    int callee_cilk_height;
};

#define INITIAL_CAPACITY 10
static cc_entry *entries = NULL;
static int number_of_entries = 0;
static int entries_capacity = 0;
#define INVALID_CC_ID (-1)

/* The hashtable portion is implemented in a way that uses chaining for
   collisions. */
typedef struct cc_hashtable_entry cc_hashtable_entry;

struct cc_hashtable_entry {
    cc_id_t id;
    cc_hashtable_entry *next;
};

#define HASHTABLE_SIZE (1 << 15)
static cc_hashtable_entry *hashtable[HASHTABLE_SIZE];

void __cilkrts_cc_hashtable_initialize() {
    lock = __cilkrts_os_mutex_create();
    memset(hashtable, 0, sizeof(hashtable));
}

void __cilkrts_cc_hashtable_deinitialize() {
    __cilkrts_os_mutex_destroy(lock);
    __cilkrts_free(entries);

    for (int i = 0; i < HASHTABLE_SIZE; i++) {
        while (hashtable[i] != NULL) {
            /* Free the head of the linked list. */
            cc_hashtable_entry *hashtable_entry = hashtable[i];
            hashtable[i] = hashtable_entry->next;
            __cilkrts_free(hashtable_entry);
        }
    }
}

/* A pseudo-random looking 64-bit number. */
#define INITIAL_VALUE 303986715369922901ULL

static int hash(void *caller,
                void *callee,
                int callee_cilk_height) {
    uint64_t value = (uint64_t)(caller) ^ INITIAL_VALUE;
    value = value * (2 * value + 1);
    value = (value << 32) | (value >> 32);
    value ^= (uint64_t)(callee);
    value = value * (2 * value + 1);
    value = (value << 32) | (value >> 32);
    value ^= (uint64_t)(callee_cilk_height);
    value = value * (2 * value + 1);
    return ((value >> 7) % HASHTABLE_SIZE);
}

static cc_id_t add_to_hashtable(void *caller,
                                void *callee,
                                int callee_cilk_height,
                                int hash_value) {
    /* Insert the new value into the entries list. */
    if (entries_capacity == 0) {
        entries_capacity = INITIAL_CAPACITY;
        entries = __cilkrts_malloc(entries_capacity * sizeof(cc_entry));
    } else if (entries_capacity <= number_of_entries) {
        entries_capacity *= 2;
        entries = __cilkrts_realloc(entries, entries_capacity * sizeof(cc_entry));
    }

    cc_id_t new_id = number_of_entries;
    entries[new_id].caller = caller;
    entries[new_id].callee = callee;
    entries[new_id].callee_cilk_height = callee_cilk_height;
    number_of_entries++;

    /* Insert the new value into the hashtable. */
    cc_hashtable_entry *new_hashtable_entry =
            __cilkrts_malloc(sizeof(cc_hashtable_entry));
    new_hashtable_entry->id = new_id;
    new_hashtable_entry->next = hashtable[hash_value];
    hashtable[hash_value] = new_hashtable_entry;

    return new_id;
}

cc_id_t __cilkrts_cc_hashtable_get_id(void *caller,
                                      void *callee,
                                      int callee_cilk_height) {
    __cilkrts_os_mutex_lock(lock);

    /* Look in the hashtable. */
    int hash_value = hash(caller,
                          callee,
                          callee_cilk_height);

    cc_id_t id = INVALID_CC_ID;

    for (cc_hashtable_entry *node = hashtable[hash_value];
         node != NULL;
         node = node->next) {

        if (entries[node->id].caller == caller &&
            entries[node->id].callee == callee &&
            entries[node->id].callee_cilk_height == callee_cilk_height) {

            id = node->id;
            break;
        }
    }

    if (id == INVALID_CC_ID) {
        id = add_to_hashtable(caller,
                              callee,
                              callee_cilk_height,
                              hash_value);
    }

    __cilkrts_os_mutex_unlock(lock);
    return id;
}

/* TODO(kxing): Check if locking is actually necessary for these getters. */

void *__cilkrts_cc_hashtable_fetch_caller(cc_id_t id) {
    __cilkrts_os_mutex_lock(lock);
    void *caller = entries[id].caller;
    __cilkrts_os_mutex_unlock(lock);

    return caller;
}

void *__cilkrts_cc_hashtable_fetch_callee(cc_id_t id) {
    __cilkrts_os_mutex_lock(lock);
    void *callee = entries[id].callee;
    __cilkrts_os_mutex_unlock(lock);

    return callee;
}

int __cilkrts_cc_hashtable_fetch_callee_cilk_height(cc_id_t id) {
    __cilkrts_os_mutex_lock(lock);
    int callee_cilk_height = entries[id].callee_cilk_height;
    __cilkrts_os_mutex_unlock(lock);

    return callee_cilk_height;
}
