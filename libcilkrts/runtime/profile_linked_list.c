#include "internal/profile_linked_list.h"

#include <limits.h>

#include "bug.h"
#include "cilk_malloc.h"

__cilkrts_profile_linked_list_node *
        __cilkrts_create_profile_linked_list_node() {
    __cilkrts_profile_linked_list_node *node =
            __cilkrts_malloc(sizeof(__cilkrts_profile_linked_list_node));
    node->time = 0;

    /* This value should be overwritten elsewhere. Hopefully the default value
       will cause a segfault when indexed into an array, so that we can
       fail-fast on error. */
    node->id = INT_MIN;

    node->next = NULL;
}

void __cilkrts_destroy_profile_linked_list_node(
        __cilkrts_profile_linked_list_node *node) {
    __cilkrts_free(node);
}

__cilkrts_profile_linked_list *__cilkrts_create_profile_linked_list() {
    __cilkrts_profile_linked_list *list =
            __cilkrts_malloc(sizeof(__cilkrts_profile_linked_list));
    list->head = NULL;
    list->tail = NULL;
    list->size = 0;
    list->largest_id = -1;
    return list;
}

void __cilkrts_destroy_profile_linked_list(
        __cilkrts_profile_linked_list *list) {
    if (list != NULL && list->size > 0) {
        /* Destroy the nodes that are still floating around. */
        CILK_ASSERT(list->head != NULL);
        CILK_ASSERT(list->tail != NULL);

        while (list->head != NULL) {
            __cilkrts_profile_linked_list_node *next_node = list->head->next;
            __cilkrts_destroy_profile_linked_list_node(list->head);
            list->head = next_node;
            (list->size)--;
        }

        CILK_ASSERT(list->size == 0);
    }
    __cilkrts_free(list);
}
