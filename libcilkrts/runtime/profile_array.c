#include "internal/profile_array.h"

#include "runtime/cilk_malloc.h"

__cilkrts_profile_array *__cilkrts_create_profile_array() {
    __cilkrts_profile_array *array =
            __cilkrts_malloc(sizeof(__cilkrts_profile_array));
    array->times = NULL;
    array->capacity = 0;
    return array;
}

void __cilkrts_destroy_profile_array(__cilkrts_profile_array *array) {
    if (array == NULL) {
        return;
    }

    __cilkrts_free(array->times);
    __cilkrts_free(array);
}

void __cilkrts_expand_profile_array(__cilkrts_profile_array *array,
                                    int minimum_capacity) {
    if (array->capacity >= minimum_capacity) {
        return;
    }
    int new_capacity;
    if (2 * array->capacity >= minimum_capacity) {
        new_capacity = 2 * array->capacity;
    } else {
        new_capacity = minimum_capacity;
    }
    array->times = __cilkrts_realloc(array->times,
                                     new_capacity * sizeof(long long));

    /* TODO(kxing): Change this into a memset for better performance. */
    for (int i = array->capacity; i < new_capacity; i++) {
        array->times[i] = 0;
    }

    array->capacity = new_capacity;
}
