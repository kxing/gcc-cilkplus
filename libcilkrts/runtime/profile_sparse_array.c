#include <stdbool.h>
#include "bug.h"
#include "cilk_malloc.h"
#include "internal/profile_sparse_array.h"

const static int PROMOTION_THRESHOLD = 10;

__cilkrts_profile_sparse_array *__cilkrts_create_profile_sparse_array() {
    __cilkrts_profile_sparse_array *spa =
            __cilkrts_malloc(sizeof(__cilkrts_profile_sparse_array));

    spa->array = NULL;
    spa->list = __cilkrts_create_profile_linked_list();

    return spa;
}

void __cilkrts_destroy_profile_sparse_array(
        __cilkrts_profile_sparse_array *spa) {

    if (spa == NULL) {
        return;
    }

    __cilkrts_destroy_profile_array(spa->array);
    __cilkrts_destroy_profile_linked_list(spa->list);

    __cilkrts_free(spa);
}

/* Sums two arrays. Destroys the input arrays and returns an allocated array. */
static __cilkrts_profile_array *sum_two_arrays(
        __cilkrts_profile_array *array1,
        __cilkrts_profile_array *array2) {

    if (array1 == NULL) {
        return array2;
    } else if (array2 == NULL) {
        return array1;
    }

    __cilkrts_profile_array *big_array = array1;
    __cilkrts_profile_array *small_array = array2;

    if (array1->capacity < array2->capacity) {
        big_array = array2;
        small_array = array1;
    }

    /* Add in the elements of the small array to the bigger array. */
    for (int id = 0; id < small_array->capacity; id++) {
        big_array->times[id] += small_array->times[id];
    }

    /* Memory management. */
    __cilkrts_destroy_profile_array(small_array);
    return big_array;
}

/* Sums two lists. Destroys the input lists and returns an allocated list. */
static __cilkrts_profile_linked_list *sum_two_lists(
        __cilkrts_profile_linked_list *list1,
        __cilkrts_profile_linked_list *list2) {

    if (list1 == NULL) {
        return list2;
    } else if (list2 == NULL) {
        return list1;
    }

    /* Accumlate the result into list1. */
    if (list2->head == NULL) {
        CILK_ASSERT(list2->tail == NULL);
        CILK_ASSERT(list2->size == 0);
    } else if (list1->head == NULL) {
        CILK_ASSERT(list1->tail == NULL);
        CILK_ASSERT(list1->size == 0);
        list1->head = list2->head;
        list1->tail = list2->tail;
    } else {
        /* Concatenate the lists. */
        CILK_ASSERT(list1->tail != NULL);
        list1->tail->next = list2->head;
        list1->tail = list2->tail;
    }
    list1->size += list2->size;
    list2->head = NULL;
    list2->tail = NULL;
    list2->size = 0;
    if (list1->largest_id < list2->largest_id) {
        list1->largest_id = list2->largest_id;
    }

    /* Memory management */
    __cilkrts_destroy_profile_linked_list(list2);
    return list1;
}

static void add_list_to_array(__cilkrts_profile_linked_list *list,
                              __cilkrts_profile_array *array) {
    if (list == NULL) {
        return;
    }

    __cilkrts_profile_linked_list_node *node = list->head;

    while (node != NULL) {
        CILK_ASSERT(node->id >= 0);

        /* Add the node information to the array. */
        if (array->capacity <= node->id) {
            /* Note: This code could be optimized, so this only has to be
                     called once. */
            /* TODO(kxing): Optimize this. */
            __cilkrts_expand_profile_array(array, node->id + 1);
        }
        array->times[node->id] += node->time;

        /* Delete the current node and advance. */
        __cilkrts_profile_linked_list_node *next_node = node->next;
        __cilkrts_destroy_profile_linked_list_node(node);
        (list->size)--;
        node = next_node;
    }

    list->head = NULL;
    list->tail = NULL;
    list->largest_id = -1;
    CILK_ASSERT(list->size == 0);
}

static void promote_list_to_array(__cilkrts_profile_sparse_array *spa) {
    CILK_ASSERT(spa != NULL);

    /* Create an array for the sparse array, if there isn't one already. */
    __cilkrts_profile_array *array = spa->array;

    if (array == NULL) {
        array = __cilkrts_create_profile_array();
        spa->array = array;
    }

    __cilkrts_profile_linked_list *list = spa->list;

    add_list_to_array(list, array);
}

static void consider_list_to_array_promotion(
        __cilkrts_profile_sparse_array *spa) {

    CILK_ASSERT(spa->list != NULL);

    /* Consider promoting the list into an array. */
    bool should_promote = false;

    if (spa->array == NULL && spa->list->size >= PROMOTION_THRESHOLD) {
        should_promote = true;
    } else if (spa->array != NULL &&
               spa->list->size >= spa->array->capacity &&
               spa->list->size >= spa->list->largest_id) {
        should_promote = true;
    }

    if (should_promote) {
        promote_list_to_array(spa);
    }
}

__cilkrts_profile_sparse_array *__cilkrts_profile_sparse_array_sum(
        __cilkrts_profile_sparse_array *spa1,
        __cilkrts_profile_sparse_array *spa2) {

    CILK_ASSERT(spa1 != NULL);
    CILK_ASSERT(spa2 != NULL);

    __cilkrts_profile_array *array = sum_two_arrays(spa1->array, spa2->array);
    __cilkrts_profile_linked_list *list = sum_two_lists(spa1->list, spa2->list);

    spa1->array = array;
    spa1->list = list;
    spa2->array = NULL;
    spa2->list = NULL;

    consider_list_to_array_promotion(spa1);

    __cilkrts_destroy_profile_sparse_array(spa2);
    return spa1;
}

void __cilkrts_profile_sparse_array_update(
        __cilkrts_profile_sparse_array *spa,
        int id,
        long long time) {

    CILK_ASSERT(spa != NULL);
    CILK_ASSERT(spa->list != NULL);

    __cilkrts_profile_linked_list *list = spa->list;
    __cilkrts_profile_linked_list_node *new_node =
            __cilkrts_create_profile_linked_list_node();
    new_node->time = time;
    new_node->id = id;
    new_node->next = NULL;

    /* Append the node to the end. */
    if (list->tail == NULL) {
        CILK_ASSERT(list->head == NULL);
        CILK_ASSERT(list->size == 0);
        list->head = new_node;
    } else {
        list->tail->next = new_node;
    }
    list->tail = new_node;
    (list->size)++;
    if (list->largest_id < id) {
        list->largest_id = id;
    }

    consider_list_to_array_promotion(spa);
}

void __cilkrts_profile_sparse_array_finalize(
        __cilkrts_profile_sparse_array *spa) {

    CILK_ASSERT(spa != NULL);
    CILK_ASSERT(spa->list != NULL);

    promote_list_to_array(spa);
}

long long __cilkrts_profile_sparse_array_get(
        __cilkrts_profile_sparse_array *spa,
        int id) {

    CILK_ASSERT(spa != NULL);
    CILK_ASSERT(spa->array != NULL);
    CILK_ASSERT(spa->list != NULL);
    CILK_ASSERT(spa->list->size == 0);

    if (id < spa->array->capacity) {
        return spa->array->times[id];
    } else {
        return 0;
    }
}

int __cilkrts_profile_sparse_array_get_capacity(
        __cilkrts_profile_sparse_array *spa) {

    CILK_ASSERT(spa != NULL);
    CILK_ASSERT(spa->array != NULL);
    CILK_ASSERT(spa->list != NULL);
    CILK_ASSERT(spa->list->size == 0);

    return spa->array->capacity;
}
