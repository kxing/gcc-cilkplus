#include "bug.h"
#include "cilk_malloc.h"
#include "internal/profile.h"
#include "internal/profile_node.h"

#ifdef CHECK_FOR_PROFILE_NODE_LEAKS
long long profile_nodes_allocated = 0;
#endif /* CHECK_FOR_PROFILE_NODE_LEAKS */

__cilkrts_profile_node *__cilkrts_create_profile_node() {
    __cilkrts_profile_node *profile_node =
            __cilkrts_malloc(sizeof(__cilkrts_profile_node));
    profile_node->parent = NULL;
    profile_node->spawned_child = NULL;
    profile_node->continuation_child = NULL;
    profile_node->contents = __cilkrts_create_profile();
#ifdef CHECK_FOR_PROFILE_NODE_LEAKS
    profile_nodes_allocated++;
#endif /* CHECK_FOR_PROFILE_NODE_LEAKS */
    return profile_node;
}

void __cilkrts_destroy_profile_node(__cilkrts_profile_node *profile_node) {
    CILK_ASSERT(profile_node != NULL);
    CILK_ASSERT(profile_node->contents == NULL);
    /* Profile nodes should be unlinked from everything else before being
       deleted. */
    CILK_ASSERT(profile_node->parent == NULL);
    CILK_ASSERT(profile_node->spawned_child == NULL);
    CILK_ASSERT(profile_node->continuation_child == NULL);
    __cilkrts_free(profile_node);
#ifdef CHECK_FOR_PROFILE_NODE_LEAKS
    profile_nodes_allocated--;
#endif /* CHECK_FOR_PROFILE_NODE_LEAKS */
}

void __cilkrts_spawn_link_profile_nodes(__cilkrts_profile_node *parent,
                                        __cilkrts_profile_node *child) {
    CILK_ASSERT(parent != NULL);
    CILK_ASSERT(child != NULL);

    CILK_ASSERT(child->parent == NULL);
    child->parent = parent;
    CILK_ASSERT(parent->spawned_child == NULL);
    parent->spawned_child = child;
}

void __cilkrts_continuation_link_profile_nodes(__cilkrts_profile_node *parent,
                                               __cilkrts_profile_node *child) {
    CILK_ASSERT(parent != NULL);
    CILK_ASSERT(child != NULL);

    CILK_ASSERT(child->parent == NULL);
    child->parent = parent;
    CILK_ASSERT(parent->continuation_child == NULL);
    parent->continuation_child = child;
}

void __cilkrts_unlink_profile_nodes(__cilkrts_profile_node *parent,
                                    __cilkrts_profile_node *child) {
    CILK_ASSERT(parent != NULL);
    CILK_ASSERT(child != NULL);

    if (parent->spawned_child == child) {
        parent->spawned_child = NULL;
    }
    if (parent->continuation_child == child) {
        parent->continuation_child = NULL;
    }
    child->parent = NULL;
}

void __cilkrts_set_profile_node_contents(__cilkrts_profile_node *profile_node,
                                         __cilkrts_profile *contents) {
    CILK_ASSERT(profile_node != NULL);
    CILK_ASSERT(profile_node->contents == NULL);
    profile_node->contents = contents;
}

__cilkrts_profile *__cilkrts_get_profile_node_contents(
        __cilkrts_profile_node *profile_node) {
    return profile_node->contents;
}

__cilkrts_profile *__cilkrts_remove_profile_node_contents(
        __cilkrts_profile_node *profile_node) {
    CILK_ASSERT(profile_node != NULL);

    __cilkrts_profile *contents = profile_node->contents;
    CILK_ASSERT(contents != NULL);

    profile_node->contents = NULL;
    return contents;
}

void __cilkrts_reduce_profile_node(__cilkrts_profile_node *profile_node) {
    CILK_ASSERT(profile_node != NULL);

    __cilkrts_profile_node *spawned_child = profile_node->spawned_child;
    __cilkrts_profile_node *continuation_child =
            profile_node->continuation_child;

    __cilkrts_profile *spawned_profile = NULL;
    __cilkrts_profile *continuation_profile = NULL;

    /* Fetch the profile of the spawned child, if there is one. */
    if (spawned_child != NULL) {
        spawned_profile = __cilkrts_remove_profile_node_contents(spawned_child);
        __cilkrts_unlink_profile_nodes(profile_node, spawned_child);
        __cilkrts_destroy_profile_node(spawned_child);
    }

    /* Fetch the profile of the continuation child. */
    CILK_ASSERT(continuation_child != NULL);
    continuation_profile = __cilkrts_remove_profile_node_contents(
            continuation_child);
    __cilkrts_unlink_profile_nodes(profile_node, continuation_child);
    __cilkrts_destroy_profile_node(continuation_child);

    /* Do a parallel merge for all of the children. */
    __cilkrts_profile *merged_profile = __cilkrts_parallel_merge_profiles(
            spawned_profile,
            continuation_profile);
    __cilkrts_profile *profile =
            __cilkrts_remove_profile_node_contents(profile_node);

    /* Do a series merge for the profile node with its children. */
    __cilkrts_profile *new_profile = __cilkrts_series_merge_profiles(
            merged_profile,
            profile);

    /* Update. */
    __cilkrts_set_profile_node_contents(profile_node, new_profile);

    CILK_ASSERT(profile_node->spawned_child == NULL);
    CILK_ASSERT(profile_node->continuation_child == NULL);
}
