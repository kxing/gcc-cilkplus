#include <stdbool.h>
#include <stdio.h>

#include "bug.h"
#include "cilk_malloc.h"
#include "internal/cc_hashtable.h"
#include "internal/profile.h"

#ifdef CHECK_FOR_PROFILE_LEAKS
long long profiles_allocated = 0;
#endif /* CHECK_FOR_PROFILE_LEAKS */

__cilkrts_work_span *__cilkrts_create_work_span() {
    __cilkrts_work_span *ws = __cilkrts_malloc(sizeof(__cilkrts_work_span));
    ws->work = 0;
    ws->span = 0;
    ws->next = NULL;
    return ws;
}

void __cilkrts_destroy_work_span(__cilkrts_work_span *ws) {
    __cilkrts_free(ws);
}

static __cilkrts_profile *create_new_profile(bool blank) {
    __cilkrts_profile *p = __cilkrts_malloc(sizeof(__cilkrts_profile));

    if (blank) {
        p->work_on_work_cc_spa = NULL;
        p->span_on_work_cc_spa = NULL;
        p->work_on_span_cc_spa = NULL;
        p->span_on_span_cc_spa = NULL;
        p->work_bb_spa = NULL;
        p->span_bb_spa = NULL;
        p->ws = NULL;
    } else {
        p->work_on_work_cc_spa = __cilkrts_create_profile_sparse_array();
        p->span_on_work_cc_spa = __cilkrts_create_profile_sparse_array();
        p->work_on_span_cc_spa = __cilkrts_create_profile_sparse_array();
        p->span_on_span_cc_spa = __cilkrts_create_profile_sparse_array();
        p->work_bb_spa = __cilkrts_create_profile_sparse_array();
        p->span_bb_spa = __cilkrts_create_profile_sparse_array();
        p->ws = __cilkrts_create_work_span();
    }

#ifdef CHECK_FOR_PROFILE_LEAKS
    profiles_allocated++;
#endif /* CHECK_FOR_PROFILE_LEAKS */

    return p;
}

__cilkrts_profile *__cilkrts_create_profile() {
    return create_new_profile(false);
}

void __cilkrts_destroy_profile(__cilkrts_profile *p) {
#ifdef CHECK_FOR_PROFILE_LEAKS
    profiles_allocated--;
#endif /* CHECK_FOR_PROFILE_LEAKS */

    if (p == NULL) {
        return;
    }

    __cilkrts_destroy_work_span(p->ws);
    __cilkrts_destroy_profile_sparse_array(p->work_on_work_cc_spa);
    __cilkrts_destroy_profile_sparse_array(p->span_on_work_cc_spa);
    __cilkrts_destroy_profile_sparse_array(p->work_on_span_cc_spa);
    __cilkrts_destroy_profile_sparse_array(p->span_on_span_cc_spa);
    __cilkrts_destroy_profile_sparse_array(p->work_bb_spa);
    __cilkrts_destroy_profile_sparse_array(p->span_bb_spa);

    __cilkrts_free(p);
}

void __cilkrts_update_cc_profile(__cilkrts_profile *p,
                                 cc_id_t id,
                                 long long work,
                                 long long span) {
    CILK_ASSERT(p != NULL);
    __cilkrts_profile_sparse_array_update(p->work_on_work_cc_spa, id, work);
    __cilkrts_profile_sparse_array_update(p->span_on_work_cc_spa, id, span);
    __cilkrts_profile_sparse_array_update(p->work_on_span_cc_spa, id, work);
    __cilkrts_profile_sparse_array_update(p->span_on_span_cc_spa, id, span);
}

void __cilkrts_update_bb_profile(__cilkrts_profile *p,
                                 bb_id_t id,
                                 long long work,
                                 long long span) {
    CILK_ASSERT(p != NULL);
    __cilkrts_profile_sparse_array_update(p->work_bb_spa, id, work);
    __cilkrts_profile_sparse_array_update(p->span_bb_spa, id, span);
}

__cilkrts_profile *__cilkrts_series_merge_profiles(
        __cilkrts_profile *child_profile,
        __cilkrts_profile *parent_profile) {

    CILK_ASSERT(child_profile != NULL);
    CILK_ASSERT(parent_profile != NULL);

    __cilkrts_profile *p = create_new_profile(true);

    /* Compute the new work-on-work caller-callee sparse array. */
    p->work_on_work_cc_spa = __cilkrts_profile_sparse_array_sum(
            child_profile->work_on_work_cc_spa,
            parent_profile->work_on_work_cc_spa);
    child_profile->work_on_work_cc_spa = NULL;
    parent_profile->work_on_work_cc_spa = NULL;

    /* Compute the new span-on-work caller-callee sparse array. */
    p->span_on_work_cc_spa = __cilkrts_profile_sparse_array_sum(
            child_profile->span_on_work_cc_spa,
            parent_profile->span_on_work_cc_spa);
    child_profile->span_on_work_cc_spa = NULL;
    parent_profile->span_on_work_cc_spa = NULL;

    /* Compute the new work-on-span caller-callee sparse array. */
    p->work_on_span_cc_spa = __cilkrts_profile_sparse_array_sum(
            child_profile->work_on_span_cc_spa,
            parent_profile->work_on_span_cc_spa);
    child_profile->work_on_span_cc_spa = NULL;
    parent_profile->work_on_span_cc_spa = NULL;

    /* Compute the new span-on-span caller-callee sparse array. */
    p->span_on_span_cc_spa = __cilkrts_profile_sparse_array_sum(
            child_profile->span_on_span_cc_spa,
            parent_profile->span_on_span_cc_spa);
    child_profile->span_on_span_cc_spa = NULL;
    parent_profile->span_on_span_cc_spa = NULL;

    /* Compute the new work block sparse array. */
    p->work_bb_spa = __cilkrts_profile_sparse_array_sum(
            child_profile->work_bb_spa,
            parent_profile->work_bb_spa);
    child_profile->work_bb_spa = NULL;
    parent_profile->work_bb_spa = NULL;

    /* Compute the new span block sparse array. */
    p->span_bb_spa = __cilkrts_profile_sparse_array_sum(
            child_profile->span_bb_spa,
            parent_profile->span_bb_spa);
    child_profile->span_bb_spa = NULL;
    parent_profile->span_bb_spa = NULL;

    CILK_ASSERT(child_profile->ws != NULL);
    CILK_ASSERT(parent_profile->ws != NULL);
    CILK_ASSERT(child_profile->ws->next == NULL);

    /* Compute the overall work and span. */
    parent_profile->ws->work += child_profile->ws->work;
    parent_profile->ws->span += child_profile->ws->span;
    p->ws = parent_profile->ws;
    parent_profile->ws = NULL;

    __cilkrts_destroy_profile(child_profile);
    __cilkrts_destroy_profile(parent_profile);

    return p;
}

__cilkrts_profile *__cilkrts_parallel_merge_profiles(
        __cilkrts_profile *profile1,
        __cilkrts_profile *profile2) {

    CILK_ASSERT(profile1 != NULL);
    CILK_ASSERT(profile2 != NULL);

    __cilkrts_profile *p = create_new_profile(true);

    /* Compute the new work-on-work caller-callee sparse array. */
    p->work_on_work_cc_spa = __cilkrts_profile_sparse_array_sum(
            profile1->work_on_work_cc_spa, profile2->work_on_work_cc_spa);
    profile1->work_on_work_cc_spa = NULL;
    profile2->work_on_work_cc_spa = NULL;

    /* Compute the new span-on-work caller-callee sparse array. */
    p->span_on_work_cc_spa = __cilkrts_profile_sparse_array_sum(
            profile1->span_on_work_cc_spa, profile2->span_on_work_cc_spa);
    profile1->span_on_work_cc_spa = NULL;
    profile2->span_on_work_cc_spa = NULL;

    /* Compute the new work-on-span caller-callee sparse array. */
    if (profile1->ws->span > profile2->ws->span) {
        p->work_on_span_cc_spa = profile1->work_on_span_cc_spa;
        profile1->work_on_span_cc_spa = NULL;
        __cilkrts_destroy_profile_sparse_array(profile2->work_on_span_cc_spa);
        profile2->work_on_span_cc_spa = NULL;
    } else {
        p->work_on_span_cc_spa = profile2->work_on_span_cc_spa;
        profile2->work_on_span_cc_spa = NULL;
        __cilkrts_destroy_profile_sparse_array(profile1->work_on_span_cc_spa);
        profile1->work_on_span_cc_spa = NULL;
    }

    /* Compute the new span-on-span caller-callee sparse array. */
    if (profile1->ws->span > profile2->ws->span) {
        p->span_on_span_cc_spa = profile1->span_on_span_cc_spa;
        profile1->span_on_span_cc_spa = NULL;
        __cilkrts_destroy_profile_sparse_array(profile2->span_on_span_cc_spa);
        profile2->span_on_span_cc_spa = NULL;
    } else {
        p->span_on_span_cc_spa = profile2->span_on_span_cc_spa;
        profile2->span_on_span_cc_spa = NULL;
        __cilkrts_destroy_profile_sparse_array(profile1->span_on_span_cc_spa);
        profile1->span_on_span_cc_spa = NULL;
    }

    /* Compute the new work block sparse array. */
    p->work_bb_spa = __cilkrts_profile_sparse_array_sum(
            profile1->work_bb_spa, profile2->work_bb_spa);
    profile1->work_bb_spa = NULL;
    profile2->work_bb_spa = NULL;

    /* Compute the new span block sparse array. */
    if (profile1->ws->span > profile2->ws->span) {
        p->span_bb_spa = profile1->span_bb_spa;
        profile1->span_bb_spa = NULL;
        __cilkrts_destroy_profile_sparse_array(profile2->span_bb_spa);
        profile2->span_bb_spa = NULL;
    } else {
        p->span_bb_spa = profile2->span_bb_spa;
        profile2->span_bb_spa = NULL;
        __cilkrts_destroy_profile_sparse_array(profile1->span_bb_spa);
        profile1->span_bb_spa = NULL;
    }

    CILK_ASSERT(profile1->ws != NULL);
    CILK_ASSERT(profile2->ws != NULL);
    CILK_ASSERT(profile1->ws->next == NULL);
    CILK_ASSERT(profile2->ws->next == NULL);

    /* Compute the overall work and span. */
    profile2->ws->work += profile1->ws->work;
    if (profile2->ws->span < profile1->ws->span) {
        profile2->ws->span = profile1->ws->span;
    }
    p->ws = profile2->ws;
    profile2->ws = NULL;

    __cilkrts_destroy_profile(profile1);
    __cilkrts_destroy_profile(profile2);

    return p;
}

void __cilkrts_print_cc_profile(__cilkrts_profile *p, FILE *cc_stream) {
    fprintf(cc_stream, "caller,callee,callee_cilk_height,"
                       "work_on_work,span_on_work,work_on_span,span_on_span\n");
    /* This function should only really be called at the end of a Cilk run, so
       the cost of promoting the list to an array should be amortized. */
    __cilkrts_profile_sparse_array_finalize(p->work_on_work_cc_spa);
    __cilkrts_profile_sparse_array_finalize(p->span_on_work_cc_spa);
    __cilkrts_profile_sparse_array_finalize(p->work_on_span_cc_spa);
    __cilkrts_profile_sparse_array_finalize(p->span_on_span_cc_spa);

    const int work_on_work_capacity =
            __cilkrts_profile_sparse_array_get_capacity(p->work_on_work_cc_spa);
    const int span_on_work_capacity =
            __cilkrts_profile_sparse_array_get_capacity(p->span_on_work_cc_spa);
    const int work_on_span_capacity =
            __cilkrts_profile_sparse_array_get_capacity(p->work_on_span_cc_spa);
    const int span_on_span_capacity =
            __cilkrts_profile_sparse_array_get_capacity(p->span_on_span_cc_spa);

    int max_capacity = work_on_work_capacity;
    if (max_capacity < span_on_work_capacity) {
        max_capacity = span_on_work_capacity;
    }
    if (max_capacity < work_on_span_capacity) {
        max_capacity = work_on_span_capacity;
    }
    if (max_capacity < span_on_span_capacity) {
        max_capacity = span_on_span_capacity;
    }

    for (cc_id_t id = 0; id < max_capacity; id++) {
        long long work_on_work =
                __cilkrts_profile_sparse_array_get(p->work_on_work_cc_spa, id);
        long long span_on_work =
                __cilkrts_profile_sparse_array_get(p->span_on_work_cc_spa, id);
        long long work_on_span =
                __cilkrts_profile_sparse_array_get(p->work_on_span_cc_spa, id);
        long long span_on_span =
                __cilkrts_profile_sparse_array_get(p->span_on_span_cc_spa, id);

        CILK_ASSERT(work_on_work >= span_on_work);
        CILK_ASSERT(work_on_work >= work_on_span);
        CILK_ASSERT(span_on_work >= span_on_span);
        CILK_ASSERT(work_on_span >= span_on_span);

        if (work_on_work > 0 || span_on_work > 0 ||
            work_on_span > 0 || span_on_span > 0) {

            fprintf(cc_stream,
                    "%p,%p,%d,%lld,%lld,%lld,%lld\n",
                    __cilkrts_cc_hashtable_fetch_caller(id),
                    __cilkrts_cc_hashtable_fetch_callee(id),
                    __cilkrts_cc_hashtable_fetch_callee_cilk_height(id),
                    work_on_work,
                    span_on_work,
                    work_on_span,
                    span_on_span);
        }
    }
}

void __cilkrts_print_bb_profile(__cilkrts_profile *p, FILE *bb_stream) {
    fprintf(bb_stream, "start,end,work,span\n");

    /* This function should only really be called at the end of a Cilk run, so
       the cost of promoting the list to an array should be amortized. */
    __cilkrts_profile_sparse_array_finalize(p->work_bb_spa);
    __cilkrts_profile_sparse_array_finalize(p->span_bb_spa);

    const int work_capacity =
            __cilkrts_profile_sparse_array_get_capacity(p->work_bb_spa);
    const int span_capacity =
            __cilkrts_profile_sparse_array_get_capacity(p->span_bb_spa);

    const int max_capacity = (work_capacity > span_capacity) ?
                              work_capacity : span_capacity;

    for (bb_id_t id = 0; id < max_capacity; id++) {
        long long work = __cilkrts_profile_sparse_array_get(p->work_bb_spa, id);
        long long span = __cilkrts_profile_sparse_array_get(p->span_bb_spa, id);

        CILK_ASSERT(work >= span);

        if (work > 0 || span > 0) {
            fprintf(bb_stream,
                    "%p,%p,%lld,%lld\n",
                    __cilkrts_bb_hashtable_fetch_start(id),
                    __cilkrts_bb_hashtable_fetch_end(id),
                    work,
                    span);
        }
    }

}
