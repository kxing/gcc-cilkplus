#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cilk_malloc.h"
#include "os_mutex.h"
#include "internal/bb_hashtable.h"

static os_mutex *lock;

/* The "entries" are implemented as an array. */
typedef struct bb_entry bb_entry;

struct bb_entry {
    void *start;
    void *end;
};

#define INITIAL_CAPACITY 10
static bb_entry *entries = NULL;
static int number_of_entries = 0;
static int entries_capacity = 0;
#define INVALID_BB_ID (-1)

/* The hashtable portion is implemented in a way that uses chaining for
   collisions. */
typedef struct bb_hashtable_entry bb_hashtable_entry;

struct bb_hashtable_entry {
    bb_id_t id;
    bb_hashtable_entry *next;
};

#define HASHTABLE_SIZE (1 << 15)
static bb_hashtable_entry *hashtable[HASHTABLE_SIZE];

void __cilkrts_bb_hashtable_initialize() {
    lock = __cilkrts_os_mutex_create();
    memset(hashtable, 0, sizeof(hashtable));
}

void __cilkrts_bb_hashtable_deinitialize() {
    __cilkrts_os_mutex_destroy(lock);
    __cilkrts_free(entries);

    for (int i = 0; i < HASHTABLE_SIZE; i++) {
        while (hashtable[i] != NULL) {
            /* Free the head of the linked list. */
            bb_hashtable_entry *hashtable_entry = hashtable[i];
            hashtable[i] = hashtable_entry->next;
            __cilkrts_free(hashtable_entry);
        }
    }
}

/* A pseudo-random looking 64-bit number. */
#define INITIAL_VALUE 16134108463606498997ULL

static int hash(void *start, void *end) {
    uint64_t value = (uint64_t)(start) ^ INITIAL_VALUE;
    value = value * (2 * value + 1);
    value = (value << 32) | (value >> 32);
    value ^= (uint64_t)(end);
    value = value * (2 * value + 1);
    return ((value >> 7) % HASHTABLE_SIZE);
}

static bb_id_t add_to_hashtable(void *start, void *end, int hash_value) {
    /* Insert the new value into the entries list. */
    if (entries_capacity == 0) {
        entries_capacity = INITIAL_CAPACITY;
        entries = __cilkrts_malloc(entries_capacity * sizeof(bb_entry));
    } else if (entries_capacity <= number_of_entries) {
        entries_capacity *= 2;
        entries = __cilkrts_realloc(entries, entries_capacity * sizeof(bb_entry));
    }

    bb_id_t new_id = number_of_entries;
    entries[new_id].start = start;
    entries[new_id].end = end;
    number_of_entries++;

    /* Insert the new value into the hashtable. */
    bb_hashtable_entry *new_hashtable_entry =
            __cilkrts_malloc(sizeof(bb_hashtable_entry));
    new_hashtable_entry->id = new_id;
    new_hashtable_entry->next = hashtable[hash_value];
    hashtable[hash_value] = new_hashtable_entry;

    return new_id;
}

bb_id_t __cilkrts_bb_hashtable_get_id(void *start, void *end) {
    __cilkrts_os_mutex_lock(lock);

    /* Look in the hashtable. */
    int hash_value = hash(start, end);

    bb_id_t id = INVALID_BB_ID;

    for (bb_hashtable_entry *node = hashtable[hash_value];
         node != NULL;
         node = node->next) {

        if (entries[node->id].start == start &&
            entries[node->id].end == end) {

            id = node->id;
            break;
        }
    }

    if (id == INVALID_BB_ID) {
        id = add_to_hashtable(start, end, hash_value);
    }

    __cilkrts_os_mutex_unlock(lock);
    return id;
}

/* TODO(kxing): Check if locking is actually necessary for these getters. */

void *__cilkrts_bb_hashtable_fetch_start(bb_id_t id) {
    __cilkrts_os_mutex_lock(lock);
    void *start = entries[id].start;
    __cilkrts_os_mutex_unlock(lock);

    return start;
}

void *__cilkrts_bb_hashtable_fetch_end(bb_id_t id) {
    __cilkrts_os_mutex_lock(lock);
    void *end = entries[id].end;
    __cilkrts_os_mutex_unlock(lock);

    return end;
}
