#include <stdio.h>
#include <stdlib.h>

#include <cilk/cilk.h>
#include <cilk/cilk_api.h>

void foo1() {
  printf("turtles\n");
}

void bar1() {
  foo1();
}

void foo2() {
  cilk_spawn bar1();
}

void bar2() {
  foo2();
}

void foo3() {
  cilk_spawn bar2();
}

void bar3() {
  foo3();
}

void foo4() {
  cilk_spawn bar3();
}

int main(int argc, char* argv[]) {
  /* if (argc < 2) { */
  /*   fprintf(stderr, "Usage: %s n\n", argv[0]); */
  /*   return -1; */
  /* } */

  /* int n = atoi(argv[1]); */
  foo4();

  return 0;
}
