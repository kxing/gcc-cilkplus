#!/usr/bin/python
import argparse
import re
import subprocess

DEFAULT_BB_OUT_FILE_NAME = 'cilkprof.bb.in'
DEFAULT_CC_OUT_FILE_NAME = 'cilkprof.cc.in'
DEFAULT_BB_FILE_NAME = 'cilkprof.bb.csv'
DEFAULT_CC_FILE_NAME = 'cilkprof.cc.csv'
TEMP_FILE_NAME = 'temp.s'

def get_args():
    parser = argparse.ArgumentParser(description='Pretty prints CilkProf data')
    parser.add_argument(
            '--bb-in',
            help='The .bb.in data file that CilkProf produced (defaults to %s)' % \
                DEFAULT_BB_OUT_FILE_NAME,
            default=DEFAULT_BB_OUT_FILE_NAME)
    parser.add_argument(
            '--cc-in',
            help='The .cc.in data file that CilkProf produced (defaults to %s)' % \
                DEFAULT_CC_OUT_FILE_NAME,
            default=DEFAULT_CC_OUT_FILE_NAME)
    parser.add_argument(
            '--bb-out',
            help='The .bb data file that CilkProf will output (defaults to %s)' % \
                DEFAULT_BB_FILE_NAME,
            default=DEFAULT_BB_FILE_NAME)
    parser.add_argument(
            '--cc-out',
            help='The .cc data file that CilkProf will output (defaults to %s)' % \
                DEFAULT_CC_FILE_NAME,
            default=DEFAULT_CC_FILE_NAME)
    parser.add_argument('executable', help='The executable binary that was run')

    args = parser.parse_args()
    return args

def disassemble(executable):
    temp_file = open(TEMP_FILE_NAME, 'w')
    return_value = subprocess.call(['objdump', '-d', '-l', '-j.text', executable], stdout=temp_file)
    if return_value != 0:
        print 'An error occured while trying to disassemble %s' % executable
    temp_file.close()

def normalize_address(address):
    address = address.lstrip('0x')
    address = address.lstrip('0')
    if len(address) == 0:
        address = '0'
    return address.lower()

# Returns a dictionary of pairs of:
# address -> (function, previous_address, line_number)
def process_disassembly():
    fin = open(TEMP_FILE_NAME, 'r')

    current_function = ''
    last_address = None
    last_line_number = None

    first_function_regex = re.compile(r'^[0-9A-Fa-f]+ <(?P<function_name>[0-9A-Za-z_.]+)>:$')
    second_function_regex = re.compile(r'^(?P<function_name>[0-9A-Za-z_.]+)\(\):$')
    instruction_regex = re.compile(r'^  (?P<address>[0-9A-Fa-f]+):\t.*$')

    # The list of extensions was inspired by:
    # http://stackoverflow.com/questions/5171502/c-vs-cc-vs-cpp-vs-hpp-vs-h-vs-cxx
    line_number_regex = re.compile(r'(?P<line_number>.*\.(c|cc|cp|cpp|C|CPP|cxx|c\+\+|h|hh|hpp|H|hxx):[0-9]+).*$')

    instructions = {}

    for line in fin:
        # Try to match the first function regex.
        m1 = first_function_regex.match(line)
        if m1 is not None:
            function_name = m1.group('function_name')
            current_function = function_name
            last_line_number = None
            last_address = None
        # Try to match the second function regex.
        m2 = second_function_regex.match(line)
        if m2 is not None:
            function_name = m2.group('function_name')
            # Not actually true, due to possible inlining.
            #assert(current_function == function_name)
            #assert(last_line_number is None)
            #assert(last_address is None)
        # Try to match the instruction regex.
        im = instruction_regex.match(line)
        if im is not None:
            address = im.group('address')
            address = normalize_address(address)
            instructions[address] = (current_function, last_address, last_line_number)
            last_address = address
        # Try to match a line number.
        lnm = line_number_regex.match(line)
        if lnm is not None:
            line_number = lnm.group('line_number')
            if '\"' in line_number:
                line_number = r'"%s"' % line_number.replace(r'"', r'\"')
            last_line_number = line_number

    fin.close()
    return instructions

def process_bb_data_file(bb_in, bb_out, instructions):
    fin = open(bb_in, 'r')
    fout = open(bb_out, 'w')

    aggregate_results = {}

    for line in fin:
        line = line.rstrip('\n')
        if line == 'start,end,work,span':
            fout.write("%s,%s,%s,%s,%s,%s\n" % \
                       ('start_function', \
                        'start_line_number', \
                        'end_function', \
                        'end_line_number', \
                        'work', \
                        'span'))
        else:
            start, end, work, span = line.split(',')
            start = normalize_address(start)
            end = normalize_address(end)
            if '.' in work:
                work = float(work)
            else:
                work = int(work)
            if '.' in span:
                span = float(span)
            else:
                span = int(span)
            _, prev_start, _ = instructions[start]
            _, prev_end, _ = instructions[end]
            start_function, _, start_line_number = instructions[prev_start]
            end_function, _, end_line_number = instructions[prev_end]

            t = (start_function, start_line_number, end_function, end_line_number)
            if t not in aggregate_results:
                aggregate_results[t] = (0, 0)
            old_work, old_span = aggregate_results[t]
            aggregate_results[t] = (old_work + work, old_span + span)

    for (key, value) in aggregate_results.iteritems():
        start_function, start_line_number, end_function, end_line_number = key
        work, span = value
        fout.write("%s,%s,%s,%s,%s,%s\n" % \
                   (start_function, \
                    start_line_number, \
                    end_function, \
                    end_line_number, \
                    work, \
                    span))

    fin.close()
    fout.close()

def process_cc_data_file(cc_in, cc_out, instructions):
    fin = open(cc_in, 'r')
    fout = open(cc_out, 'w')

    aggregate_results = {}

    for line in fin:
        line = line.rstrip('\n')
        if line == 'caller,callee,callee_cilk_height,work_on_work,span_on_work,work_on_span,span_on_span':
            fout.write("%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % \
                       ('caller_function', \
                        'caller_line_number', \
                        'callee_function', \
                        'callee_line_number', \
                        'callee_cilk_height', \
                        'work_on_work', \
                        'span_on_work', \
                        'work_on_span', \
                        'span_on_span'))
        else:
            caller, callee, callee_cilk_height, work_on_work, span_on_work, \
                    work_on_span, span_on_span = line.split(',')
            caller = normalize_address(caller)
            callee = normalize_address(callee)
            if '.' in work_on_work:
                work_on_work = float(work_on_work)
            else:
                work_on_work = int(work_on_work)
            if '.' in span_on_work:
                span_on_work = float(span_on_work)
            else:
                span_on_work = int(span_on_work)
            if '.' in work_on_span:
                work_on_span = float(work_on_span)
            else:
                work_on_span = int(work_on_span)
            if '.' in span_on_span:
                span_on_span = float(span_on_span)
            else:
                span_on_span = int(span_on_span)
            _, prev_caller, _ = instructions[caller]
            _, prev_callee, _ = instructions[callee]
            caller_function, _, caller_line_number = instructions[prev_caller]
            callee_function, _, callee_line_number = instructions[prev_callee]

            t = (caller_function, caller_line_number, callee_function, callee_line_number, callee_cilk_height)
            if t not in aggregate_results:
                aggregate_results[t] = (0, 0, 0, 0)
            old_work_on_work, old_span_on_work, old_work_on_span, old_span_on_span \
                    = aggregate_results[t]
            aggregate_results[t] = (old_work_on_work + work_on_work, \
                                    old_span_on_work + span_on_work, \
                                    old_work_on_span + work_on_span, \
                                    old_span_on_span + span_on_span)

    for (key, value) in aggregate_results.iteritems():
        caller_function, caller_line_number, callee_function, callee_line_number, callee_cilk_height = key
        work_on_work, span_on_work, work_on_span, span_on_span = value
        fout.write("%s,%s,%s,%s,%s,%s,%s,%s,%s\n" % \
                   (caller_function, \
                    caller_line_number, \
                    callee_function, \
                    callee_line_number, \
                    callee_cilk_height, \
                    work_on_work, \
                    span_on_work, \
                    work_on_span, \
                    span_on_span))
    fin.close()
    fout.close()

if __name__ == '__main__':
    args = get_args()
    disassemble(args.executable)

    instructions = process_disassembly()
    process_bb_data_file(args.bb_in, args.bb_out, instructions)
    process_cc_data_file(args.cc_in, args.cc_out, instructions)
