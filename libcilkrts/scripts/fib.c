#include <stdio.h>
#include <stdlib.h>

#include <cilk/cilk.h>
#include <cilk/cilk_api.h>

int fib(int n) {
  if (n < 2) return n;
  int x,y;
  x = cilk_spawn fib(n-1);
  y = fib(n-2);
  cilk_sync;
  return x+y;
}

int main(int argc, char* argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Usage: %s n\n", argv[0]);
    return -1;
  }

  int n = atoi(argv[1]);
  int fibn = fib(n);

  printf("fib(%d) = %d\n", n, fibn);

  return 0;
}
