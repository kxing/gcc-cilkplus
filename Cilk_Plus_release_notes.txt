Release Notes for Intel(R) Cilk(TM) Plus for GCC Release 1.0 

This release of Intel Cilk Plus language extensions for GCC implements the 
following features from the Intel Cilk Plus Language Specification:
* Keywords for Tasking: _Cilk_spawn, _Cilk_sync, and _Cilk_for, as well as 
  the pragma to allow users to control the grainsize of the _Cilk_for loop.
* Hyperobjects: Full support for reducers is provided for both C and C++.
* SIMD Loops : Pragma SIMD support and the following clauses:
	- vectorlength
	- linear
	- assert
	- reduction
	- private
   Note: These pragmas are supported for the innermost loops only.
* Array Notations: Array notations are supported for C and C++. Along with 
  this, we have also implemented the following builtin reduction functions:
        - __sec_reduce_add
        - __sec_reduce_mul
        - __sec_reduce_max
        - __sec_reduce_min
        - __sec_reduce_max_ind
        - __sec_reduce_min_ind
        - __sec_reduce_all_zero
        - __sec_reduce_all_nonzero
        - __sec_reduce_any_zero
        - __sec_reduce_any_nonzero
        - __sec_implicit_index     
* Elemental functions: Elemental functions allow the compiler to create vector
  version of a function so that the vectorizer can use them to vectorize
  functions.

Known Issues:
* Exception support - Exceptions are not supported for Cilk keywords.

----------------------------------------------------------------------------
Intel and Cilk are trademarks of Intel Corporation in the U.S. and/or other 
countries.

