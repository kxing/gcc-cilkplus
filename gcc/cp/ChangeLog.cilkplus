2013-04-05  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-array-notation.c (build_x_array_notation_expr):  Made checking for
	invariant expression inn RHS of an array notation non-conditional.
	Also, extracted tripets of array notations to follow the execute once
	rule.  Finally, added support to allow array_refs in between array
	notations.
	(find_rank): Changed a for-loop to a while loop to accomodate array ref
	between array notation exprs.

2013-03-08  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-gimplify.c (genericize_cp_loop): Removed calls to the functions
	pragma_simd_create_private_vars and psv_find_node.

2013-02-27  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-gimplify.c (cp_gimplify_init_expr): Copied the _Cilk_spawn func.
	call marker into the appropriate return expression.
	* semantics.c (simplify_aggr_init_expr): Likewise.

2013-02-05  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* mangle.c (write_encoding): Removed adding the return type to the
	mangled name of a Cilk function.

2013-02-04  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-cilk.c (insert_sync_stmt_after_catch): New function.
	(cp_install_body_with_frame_cleanup): Likewise.
	(cp_make_cilk_frame): called walk_tree with insert_sync_stmt_after_catch
	as a function call parameter.
	* cp-objcp-common.h (LANG_HOOKS_FRAME_CLEANUP): New define.
	* except.c (do_begin_catch): Made this routine non-static.
	(do_end_catch): Likewise.

2013-01-25  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* semantics.c (finish_return_stmt): Added a check if return expression
	is a builtin array notation function.  If so, then don't issue an
	error.
	* cp-array-notation.c (find_rank): Added a FUNCTION_DECL case.
	(fix_array_notation_exprs): Added a RETURN_EXPR case.
	(fix_return_expr): New function.
	* call.c (build_over_call): Add a check if the function is a built-in
	array notation function call.  If so, then don't do any type-casting.

2013-01-09  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* semantics.c (finish_call_expr): Added a check for lambda expression 
	that is spawned.  If so, then we store it to a variable and spawn that.
	* mangle.c (write_encoding): Added a check to see if the function is a
	cilk helper function.
	(write_qualified_name): Likewise.
	(write_bare_function_type): Likewise.
	* lex.c (unqualified_fn_lookup_error): Removed an unnecessary if-stmt.
	* cp-cilk.c (cilk_lambda_fn_temp): New function.

2013-01-02  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_cilk_for_expression_iterator): Uncapitalized an
	error message and added appropriate location to error message.
	(cp_parser_jump_statement): Likewise.
	* cp-cilk.c (copy_decl_for_cilk): Likewise.

2012-12-06  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* typeck.c (cp_build_array_ref): Added a check if array index has
	array notations and if so then emit error if its rank is greater than 1.
	* cp-array-notation.c (struct inv_list): New struct.
	(find_rank): Changed *rank from int to size_t.
	(extract_array_notation_exprs): Removed all variables that used realloc 
	with vecs and replaced all insertion with the appropriate push function.
	(replace_array_notations): Likewise.
	(contains_array_notation_expr): Likewise.
	(build_array_notation_expr): All the changes as above plus replace
	several integer type variables to size_t.  Also added location variables
	when available instead of plane UNKNOWN_LOCATION.  Finally, replaced
	malloc and free with the appropriate builtin GCC's equivalent.
	(fix_conditional_array_notations_1): Likewise.
	(fix_array_notation_expr): Likewise.
	(fix_array_notation_call_expr): Likewise.
	(fix_builtin_array_notation_fn): All the changes as the above functions
	plus added support for MUTATING function.
	(is_builtin_array_notation_fn): Added support for MUTATING function.
	(find_correct_array_notation_type): Added a check for builtin array
	notation function.
	(find_inv_trees): New function.
	(replace_inv_trees): Likewise.
	(replace_invariant_exprs): Likewise.
	(build_array_notation_ref): Likewise.
	* semantics.c (finish_return_stmt): Added a check if array notation is
	present in the return statement.
	(cxx_eval_constant_expression): Added ARRAY_NOTATION_REF case.
	(potential_constant_expression_1): Likewise.
	* pt.c (tsubst_copy_and_build): Added a ARRAY_NOTATION_REF case.
	* parser.c (cp_parser_postfix_open_square_expression): Added a check for
	array notation having a braced list index.
	(cp_parser_selection_statement): Added a check for array notation inside
	condition.  If so, then emit an error message.
	(cp_parser_for): Likewise.
	(cp_parser_iteration_statement): Likewise.
	(cp_parser_direct_declarator): Added a check for array notations in a
	declarator.  If so, then emit an error message.
	(cp_parser_function_definition_after_declarator): Added a check for 
	array notations in function tree.  If so, then we fix them up.
	(cp_parser_array_notation): Added several checks for possible syntax
	errors and emit the appropriate error messages.  Also, wrapped several of
	the tasks in the function build_array_notation_ref and called it.
	* cp-objcp-common.c (cp_common_init_ts): Mark ARRAY_NOTATION_REF as
	TYPED.
	* cp-tree.h: Added several array notation function definitions.
	* lex.c (unqualified_fn_lookup_error): If cilk is enabled then  we just
	return the name.

2012-11-28  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-array-notation.c (fix_conditional_array_notations_1): Added code to
	handle array notation builtin functions inside a condition of a 
	conditional statement.
	(find_correct_array_notation_type): New function.
	* typeck.c (cp_build_binary_op): Added a check for array notations 
	inside op0 and op1.  If so, then we call the function 
	find_correct_array_notation_type.

2012-10-17  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-gimplify.c (genericize_cilk_for_stmt): Walk through all the
	_Cilk_for components that are visible by the outer function.

2012-10-09  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* semantics.c (simplify_aggr_init_expr): Carried over the Cilk_spawn's
	detach point when converting aggr_init_expr to a call_expr.

2012-09-11  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cilk.c: Deleted file.
	* cp-cilk.c: New file.
	* Make-lang.in: Renamed all occurances of cp/cilk.* to cp/cp-cilk.*.

2012-08-21  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cilk.c (gimplify_cilk_for_stmt_1): Extracted variables and added them
	to decl map.

2012-08-17  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_postfix_expression): Added a check for empty
	current function decl.
	* cilk.c (cp_build_cilk_for_body): Added a check if loop variable type
	is of integer.  If so, then we remove an cast which helps vectorize the
	body.
	(compute_loop_var): Likewise.

2012-08-11  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-array-notation.c (build_x_array_notation_expr): Added a check for
	call exprs that are builtin array notation functions.  Also moved
	around code to handle builtin array notation functions.
	(build_x_reduce_expr): Remove.
	(fix_builtin_array_notation_fn): Replaced build_decl with
	create_tmp_var in a couple places.

2012-08-10  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* decl.c (finish_function_body): Added compstmt as a new parameter.
	(cp_make_cilk_frame): Added compstmt as a parameter and called
	insert_sync_stmt inside cp_walk_tree.
	(insert_sync_stmt): New function.

2012-08-07  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_cilk_for): Stored the initial value in cilk_for tree.

2012-08-01  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_userdef_char_literal): Replaced CALL_SPAWN and
	CALL_NORMAL with CILK_CALL_SPAWN and CILK_CALL_NORMAL.
	(cp_parser_userdef_numeric_literal): Likewise.
	(cp_parser_userdef_string_literal): Likewise.
	(cp_parser_postfix_expression): Likewise.
	(cp_parser_lambda_introducer): Likewise.
	(cp_parser_perform_range_for_lookup): Likewise.
	(cp_parser_range_for_member_function): Likewise.
	(cp_parser_decltype): Likewise.
	(cp_parser_template_argument): Likewise.
	(cp_parser_omp_for_incr): Likewise.
	(cp_parser_omp_for_loop): Likewise.
	(cp_parser_userdef_char_literal): Likewise.
	* decl2.c (generate_ctor_or_dtor_function): Likewise.
	(build_offset_ref_call_from_tree): Likewise.
	* semantics.c (finish_call_expr): Likewise.
	(finish_id_expression): Likewise.
	(finish_omp_flush): Likewise.
	(finish_omp_taskwait): Likewise.
	(cxx_eval_vec_init_1): Likewise.
	(maybe_add_lambda_conv_op): Likewise.
	* pt.c (tsubst_copy_and_build): Likewise.
	* typeck2.c (split_nonconstant_init_1): Likewise.
	(build_functional_cast): Likewise.
	* cp-gimplify.c (genericize_eh_spec_block): Likewise.
	(cxx_omp_clause_apply_fn): Likewise.
	* tree.c (build_vec_init_elt): Likewise.
	* except.c (build_exc_ptr): Likewise.
	(do_get_exception_ptr): Likewise.
	(do_end_catch): Likewise.
	(do_free_exception): Likewise.
	(build_throw): Likewise.
	* rtti.c (throw_bad_cast): Likewise.
	(throw_bad_typeid): Likewise.
	(build_dynamic_cast_1): Likewise.
	* cilk.c (callable): Likewise.
	* method.c (do_build_copy_assign): Likewise.
	(locate_fn_flags): Likewise.
	* decl.c (register_dtor_fn): Likewise.
	(expand_static_init): Likewise.
	(cxx_maybe_build_cleanup): Likewise.
	* init.c (build_value_init): Likewise.
	(expand_cleanup_for_base): Likewise.
	(expand_default_init): Likewise.
	(build_builtin_delete_call): Likewise.
	(build_new_1): Likewise.
	(push_base_cleanups): Likewise.
	* optimize.c (build_delete_destructor_body): Likewise.
	* typeck.c (build_function_call): Likewise.
	(build_function_call_vec): Likewise.
	(cp_build_modify_expr): Likewise.

2012-07-30  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* pt.c (tsubst_expr): Fixed comments and spacing.  Also removed unwanted
	curly braces.
	* semantics.c (build_constexpr_constructor_member_initializers): Added
	clarifying comments.  Also fixed spacing.
	(cxx_eval_call_expression): Likewise.
	(cp_build_addr_expr_1): Added a check for cilkplus flag.
	* dwarf2out.c (dwarf2out_function_decl): Fixed a comment.
	(dwarf2out_var_location): Added a clarifying comment.
	* parser.c (cp_lexer_new_main): Added cilkplus prefix for all the
	cilkplus specific variables.  Also called the function
	pragma_simd_list.
	(cp_parser_nested_name_specifier_opt): Fixed comments and spacing.
	(cp_parser_cilk_for): Likewise.
	(cp_parser_cilk_grainsize): Likewise.
	(cp_parser_cilk_for_expression_iterator): Likewise.
	(cp_parser_simd_vectorlength): Added cilkplus prefix for all cilkplus
	specific variables.  Also fixed spacing and comments.
	(cp_parser_simd_private): Likewise.
	(cp_parser_simd_reduction): Likewise.
	(cp_parser_simd_linear): Likewise.
	(cp_parser_simd_assert): Likewise.
	* decl.c (pop_label): Removed unnecessary curly braces.
	(my_strcmp): Remove.
	(is_cilk_function_decl): Likewise.
	(duplicate_decls): Removed unwanted parenthesis.
	(store_parm_decl): Replaced static back to this function.
	(finish_function): Fixed comments and removed unwanted curly braces.
	* cp-gimplify.c (genericize_cp_loop): Added cilkplus for all variables
	that directly impact cilkplus.  Also removed unwanted curly braces.
	(genericize_for_stmt): Likewise.
	(genericize_cilk_for_stmt): Fixed comments.
	(cp_gimplify_expr): Fused two nested if-statements.
	* call.c (prep_operand): Added a check if cilk plus flag is enabled.

2012-07-23  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cilk.c (cilk_c_declare_looper): Added header comment and did some
	minor spacing and commenting issues to conform to the coding standard.
	(initialize_cilk_for_desc): Likewise.
	(release_cilk_for_desc): Likewise.
	(add_incr): Likewise.
	(resolve_continue_stmts): Likewise.
	(cilk_loop_convert): Likewise.
	(copy_decl_for_cilk): Likewise.
	(cilk_outline): Likewise.
	(declare_for_loop_variables): Likewise.
	(compute_loop_var): Likewise.
	(cp_recognize_spawn): Likewise.
	(cp_make_cilk_frame): Likewise.
	(cilk_erase_for): Likewise.
	(callable): Likewise.
	(check_limit_record): Likewise.
	(var_mentioned_p_cb): Likewise.
	(var_mentioned_p): Likewise.
	(check_incr): Likewise.
	(check_limit_scalar): Likewise.
	(validate_for_scalar): Likewise.
	(validate_for_record): Likewise.
	(check_loop_difference_type): Likewise.
	(cilk_validate_for): Likewise.
	(add_variable): Likewise.
	(extract_free_variables): Likewise.
	(gimplify_cilk_for_1): Likewise.
	(mangle_count): Likewise.
	(compute_loop_count): Likewise.
	(cp_extract_for_fields): Likewise.
	(gimplify_cilk_for_stmt): Likewise.
	(tree_uses_cilk): Likewise.
	(cilkish_type): Likewise.
	(cilk_block_local_label): Likewise.
	(cp_build_cilk_for_body): Fixed spacing, added header comment, removed
	unwanted curly brances, modified existing comments to conform to the
	coding standard and removed all cilk_linkage occurances.
	(race_warning): Activated this function (i.e., removed the #if 0 inside
	it.).
	(call_graph_add_fn): Renamed cg_hacks to this.  Also removed some
	unwanted comments, curly braces and fixed comments.
	(build_cilk_run_body): Remove.
	(cilk_gimplify_run): Likewise.
	(build_cilk_run): Likewise.
	(look_for_spawn): Likewise.
	(mark_receiver_addressable): Likewise.
	(cilk_init_common): Likewise.
	(cilk_find_parent_frame): Likewise.
	(cilk_detach): Likewise.
	(cilk_make_frame_descriptor): Likewise.
	(cilk_init_frame_descriptor): Likewise.
	(cilk_promote_struct_function): Likewise.
	(cilk_promote): Likewise.
	(cilk_maybe_promote): Likewise.
	(declare_cilk_void_wrapper): Likewise.
	(push_cilk_void_wrapper): Likewise.
	(find_cilk_void_wrapper): Likewise.
	(finish_cilk_wrappers): Likewise.
	(uses_cilk_aux): Fixed spacing, added header comment, fixed comments to
	reflect appropriately and replaced all SYNC_STMT to CILK_SYNC_STMT.
	* cp-gimplify.c (cp_gimplify_init_expr): Remove CILK_RUN_EXPR checks.
	(cp_gimplify_tree): Renamed SYNC_STMT and SPAWN_STMT to CILK_SYNC_STMT
	and CILK_SPAWN_STMT, respectively.
	(cp_gimplify_expr): Likewise.
	* parser.c (cp_parser_jump_statement): Fixed a comment to conform to
	the coding standard.
	(cp_parser_cilk_for): Added header comments, removed unwanted curly
	braces, fixed spacing and modified comments to conform to the coding
	standard.
	(cp_parser_cilk_for_condition): Likewise.
	(cp_parser_cilk_for_expression_iterator): Likewise.
	(cp_parser_simd_vectorlength): Likewise.
	(cp_parser_simd_private): Likewise.
	(cp_parser_simd_reduction): Likewise.
	(cp_parser_simd_linear): Likewise.
	(cp_parser_simd_assert): Likewise.
	(cp_parser_cilk_grainsize): Likewise.
	(cp_parser_cilk_for_init_statement): Likewise.
	(cp_parser_array_notation): Likewise.
	(cp_parser_elem_fn_processor_clause): Likewise.
	(cp_parser_elem_fn_uniform_clause): Likewise.
	(cp_parser_elem_fn_vlength_clause): Likewise.
	(cp_parser_elem_fn_expression_list): Likewise.
	* pt.c (tsubst_expr): Renamed SYNC_STMT to CILK_SYNC_STMT.
	* semantics.c (begin_compound_stmt): Likewise.
	(cxx_eval_call_expression): Modified comment to conform to the coding
	standard.
	(finish_sync_stmt): Likewise.
	(finish_cilk_block): Added a comment to the header of the function.
	(begin_cilk_for_stmt): Added a header comment and fixed spacing.
	(finish_cilk_for_init_stmt): Likewise.
	(finish_cilk_for_cond): Likewise.
	* tree.c (handle_cilk_linkage_attribute): Remove.
	* typeck.c (cp_build_addr_expr_1): Modified comments to conform to the
	coding standard.
	(lvalue_or_else): Likewise.

2012-07-19  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-lang.c: Added cilk.h header.
	* cp-objcp-common.h (LANG_HOOKS_CILK_VALID_SPAWN): Defined this.
	(LANG_HOOKS_GIMPLIFY_CILK_SPAWN): Likewise.
	(LANG_HOOKS_GIMPLIFY_CILK_FOR): Likewise.
	(LANG_HOOKS_GIMPLIFYY_CILK_SYNC): Likewise.

2012-07-19  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_postfix_open_square_expression): Rearranged code
	to fix (or unbreak) the regression test error11.C.  Also added support
	for braced index list (as per trunk).
	* semantics.c (finish_id_expression): Converted the decl to the type
	of assignment.

2012-07-18  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_postfix_expression): Added check if the function
	was also a local one (as per trunk).
	* cp-tree.h (FUNCTION_NEEDS_BODY_BLOCK): Changed the '1' to
	flag_enable_cilk (since all functions when using cilk must have a body).
	* semantics.c (expand_or_defer_fn_1): Make sure the DECL_EXTERNAL is
	not set (as per trunk).

2012-07-17  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* semantics.c (massage_constexpr_body): Reverted to trunk's function
	with the only exception being using while instead of if for BIND_EXPR.

2012-07-16  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_postfix_expression): Added a check for cilkplus
	flag.
	* decl.c (finish_function_body): Likewise.

2012-07-03  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-gimplify.c (genericize_cp_loop): Removed adding the reset stmt.

2012-06-26  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_pragma): Fixed a bug by calling linear instead of
	private.
	(cp_parser_simd_linear): Set the pragma found as true.  Also added the
	linear vars into the structure.

2012-06-21  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cilk.c (resolve_continue_stmts): Added a check for cilk-for stmt.

2012-06-20  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-gimplify.c (cp_genericize_r): Added a check for cilk-for.
	(genericize_cilk_for_stmt): New function.
	* cilk.c (resolve_continue_stmts): Likewise.
	(cp_build_cilk_for_body): Added a new label to point goto for continue
	inside cilk_for.  Also did a walk_tree to fix up all these continues.
	(cg_hacks): called cp_genericize to breakup all the statements inside
	cilk-for nested function.

2012-06-17  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cilk.c (cp_build_cilk_for_body): Set CILK_FN_P field to 1.
	(cp_make_cilk_frame): Likewise.

2012-06-14  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* pt.c (tsubst_expr): Added a check for CILK_SYNC statement.

2012-06-12  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cilk.c (callable): Removed a check to add LOOKUP_COMPLAIN.

2012-06-05  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_ctor_initializer_opt_and_function_body): Added a
	check for array notation expressions. If so, then decompose them.

2012-06-04  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cilk.c (cp_make_cilk_frame): Removed adding body to the orig body's
	tree chain.

2012-05-30  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* semantics.c (finish_call_expr): Used spawning for call_type instead of
	default CALL_NORMAL to support spawned call.

2012-05-29  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* pt.c (apply_late_template_attributes): Added a check for "vector"
	attribute for elemental functions.

2012-05-24  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_elem_fn_processor_clause): Added a underscore
	between pentium and 4 for sse3, removed an extra 's' in
	sse3 for core2_duo and put underscore between core and 2 in core2.
	* cp-gimplify.c (cp_gimplify_expr): Added a check if stmt tree is valid.

2012-05-22  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-array-notation.c (fix_conditional_array_notations_1): Changed
	build_min_nt to build_min_nt_loc with location info.
	(fix_builtin_array_notation_fn): Added location info for
	build_x_conditional_expr.  Also replaced all '1' with
	tf_warning_or_error.
	* parser.c (cp_parser_cilk_init_declarator): Change decl_specifier
	location field.  Also changed decl_spec spec array check.
	(cp_parser_array_notation): Changed build_min_nt to build_min_nt_loc
	and added location info.

2012-05-08  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cilk.c (compute_loop_var): Added location info to build_new_op.
	(callable): Likewise.
	(compute_loop_count): Likewise.
	* cp-array-notation.c (build_x_array_notation_expr): Added location
	information to grok_array_decl, build_x_unary_op, build_x_binary_op.
	(fix_builtin_array_notation_fn): Likewise.
	(fix_unary_array_notation_exprs): Likewise.
	(has_call_expr_with_array_notation): Moved declaration after if stmt.
2012-04-06  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_attribute_list): Added code to catch vector
	attribute.
	(cp_parser_elem_fn_processor_clause): New function.
	(cp_parser_elem_fn_uniform_clause): Likewise.
	(cp_parser_elem_fn_vlength_clause): Likewise.
	(cp_parser_elem_fn_linear_clause): Likewise.
	(cp_parser_elem_fn_expression_list): Likewise.

2012-03-16  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* decl.c (finish_function_body): called DECL_HAS_SPAWN_P and set it to
	calls_spawns.
	* pt.c (tsubst_copy): Carried the spawn call information from one
	call expression to another.
	(tsubst_copy_and_build): Likewise.

2012-02-14  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_compound_stmt): Added a check to see if the
	compound statement has array notations inside it.

2012-02-13  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* call.c (build_conditional_expr): Added a check for ARRAY_NOTATION_REF.
	(convert_like_real): Added check for array notations inside function.
	* cp-array-notation.c (fix_builtin_array_notation_fn): Added a
	REDUCE_ANY_NONZEROS, REDUCE_ALL_NONZEROS and REDUCE_CUSTOM case.
	(is_builtin_array_notation_fn): Replaced all occurances of "zeros" in
	string with "zero."  Also, added a check for REDUCE_CUSTOM,
	REDUCE_ALL_ZEROS, REDUCE_ALL_NONZEROS.  Also added a check for
	FUNCTION_DECL.
	(find_rank): Changed a not equal to check to a less than check.  Also
	added check for TREE_LIST and DECL_EXPR.
	(extract_array_notation_exprs): Likewise.
	(replace_array_notations): Likewise.  Also added a type check.
	(has_call_expr_with_array_notation): New function.
	(build_x_array_notations): Moved lhs_var creation and checking for
	implicit_index function to the top.  Added a check for ARRAY NOTATION
	REF.  Also replaced all TREE CONSTANT calls with specific functions.
	Replaced all build_decls with create_tmp_var.  Added a label name for
	all the label decls.  Replaced all C function-calls with their C++
	equivalents (e.g replace build_modify_expr with build_x_modify_expr).
	Also, replaced stmt list implemention to a push_stmt_list impl.
	Replaced conditional_expr in the array notation loop with a if-then-else
	statment functions.
	(fix_conditional_array_notations_1): Likewise.
	(fix_unary_array_notation_exprs): Likewise.
	(fix_builtin_array_notation_fn): All the changes in
	build_x_array_notation_exprs are incorporated into this function.  Also,
	added check for REDUCE_CUSTOM, REDUCE_ALL_NONZEROS and
	REDUCE_ANY_NONZEROS. Replaced a big case statement with if then else.
	(fix_array_notation_exprs): Added a check for DECL_EXPR, MODIFY_EXPR,
	CALL_EXPR and MODOP_EXPR.
	* parser.c (cp_parser_assignment_expression): Removed call for
	build_x_array_notation_expr and the new_expr variable.
	(cp_parser_array_notation): Added support for templates.
	(tsubst_copy_and_build): Added a ARRAY_NOTATION_REF case.
	(type_unification_real): Likewise.
	* tree.c (lvalue_kind): Likewise.
	* typeck.c (cp_build_addr_expr_1): Likewise.
	* expr.c (array_ref_element_size): Broke up a value into smaller parts.
	(array_ref_lower_bound): Likewise.

2012-01-31  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-array-notation.c (build_x_array_notation_expr): Added another
	dimension to lhs parameters to handle scatter of array notations.  Also
	replaced all cases of build_int_cst with 0 or 1 with build_zero_cst and
	build_one_cst, respectively.
	(fix_conditional_array_notations_1): Replaced build_int_cst with 0 or 1
	with build_zero_cst and build_one_cst, respectively.
	(fix_builtin_array_notation_fn): Likewise.
	(fix_unary_array_notation_exprs): Likewise.

2012-01-26  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-array-notation.c (fix_builtin_array_notation_fn): Added new var
	called "array_ind_value," that stores the max/min value and the *new_var
	stores the index.  Also, replaced all build_int_cst with build_one_cst
	and build_zero_cst.  Also, in REDUCE_ANY_ZEROS and REDUCE_ALL_ZEROS
	cases, replaced comparing with func_parm with a new_cond_expr.  Also
	initialized the maximum/minimum value for the max/min index builtin
	function.
	(contains_array_notation_expr): Deferenced array_list in if statement.
	* typeck.c (convert_for_assignment): Checked if the rhs contains array
	notation expr, if so we don't do any conversion.

2012-01-26  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-array-notation.c (extract_array_notation_exprs): Added a check
	to see if the call expr is sec_implicit_index function.
	(replace_array_notations): Likewise.
	(build_x_array_notation_expr): Likewise.

2012-01-22  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-array-notation.c (is_builtin_array_notation_fn): Initialized
	function_name to NULL.

2012-01-22  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-array-notation.c (find_rank): Removed a check for AGGR_INIT_EXPR
	with CALL_EXPR.
	(extract_array_notation_exprs): Likewise.
	(replace_array_notations): Likewise.

2012-01-20  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* call.c (convert_like_real): Type conversion is ignored if array
	notation is present as a parameter.
	* cp-array-notation.c (find_rank): Added a new parameter called
	ignore_builtin_fn.  Also handled a case for STATEMENT_LIST.  Finally,
	added a check to see if the builtin function should be ignored.
	(extract_array_notation_exprs): Added a new parameter called
	ignore_builtin_fn.  Also added a check to see for ignore_builtin_fn.
	(replace_array_notations): Likewise.
	(build_x_array_notation_expr): Modified find_rank call to add a bool.
	Also added a check if the funtion is a builtin array notation function.
	Added a bool parameter to extract_array_notation_exprs, find_rank and
	replace_array_notations.
	(fix_builtin_array_notation_fn): New function.
	(is_builtin_array_notation_fn): Likewise.
	(build_x_reduce_expr): Likewise.
	(contains_array_notation_expr): Likewise.
	(fix_unary_array_notation_exprs): Changed the name of the parameter.
	Also handled a case check for CALL_EXPR and if so, called
	fix_builtin_array_notation_fn.

2012-01-17  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-array-notation.c (max): New function.
	(fix_unary_array_notation_exprs): Likewise.
	(build_x_array_notation_expr): Added a check to see if we get an
	expression has a rank of 0.  Also added code to handle a case where
	a function call with array notation as a parameter that results a
	scalar value.  Also added a check if lhs_rank is zero.  Changed
	modifycode to NOP_EXPR.  Replaced all lhs_rank in for-loop conditions
	to max of lhs and rhs rank.  Added a check to see if array_expr_lhs is
	null, if so then just use lhs.
	(fix_conditional_array_notations): Renamed to fix_array_notation_exprs.
	Also added a case for POSTINCREMENT_EXPR, POSTDECREMENT_EXPR,
	PREDECREMENT_EXPR, PREINCREMENT_EXPR.
	* cp-tree.h: Renamed fix_conditional_array_notations to
	fix_array_notation_exprs.
	* parser.c (cp_parser_assignment_expression): Modified to handle a
	function-call that has array-notation as a parameter which will return
	a single scalar value.
	(cp_parser_compound_statement): Renamed fix_conditional array_notations
	to fix_array_notation_exprs and make sure it is called only if
	flag_enable_cilk is set.
	* typeck.c (lvalue_or_else): Removed a error check for
	ARRAY_NOTATION_REF, if flag_enable_cilk is defined.
	(cp_build_addr_expr_1): Likewise.

2012-01-12  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-array-notation.c (fix_conditional_array_notations): New function.
	(fix_conditional_array_notations_1): Likewise.
	* parser.c (cp_parser_compound_statement): Added a call to
	fix_conditional_array_notations.
	* cp-tree.h: Added a prototype for fix_conditional_array_notations.

2012-01-11  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* Make-lang.in: Added cp-array-notation.c file information.
	* cp-array-notation.c (replace_array_notations): New function.
	(find_rank): Likewise.
	(extract_array_notation_exprs): Likewise.
	(build_x_array_notation_exprs): Likewise.
	* parser.c (cp_parser_array_notation): Likewise.
	(cp_parser_nested_name_specifier_opt): Added a check to see if
	cilkplus is enabled.
	(cp_parser_postfix_open_square_expression): Added a check for CPP_COLON
	to handle array notation.
	(cp_parser_assignment_expression): Added a check for ARRAY_NOTATION_REF
	and if so, then call build_x_array_notation_expr function.
	* cp-tree.h: Added prototype for build_x_array_notation_expr.

2011-11-11  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cilk.c (for_local_cb): Changed a const tree cast to (tree *).
	(wrapper_local_cb): Likewise.

2011-11-11  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_userdef_char_literal): Added CALL_NORMAL
	parameter to finish_call_expr function call.
	(cp_parser_userdef_numeral_literal): Likewise.
	(cp_parser_userdef_string_literal): Likewise.

2011-10-22  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* typeck2.c (split_nonconstant_init_1): Added "CALL_NORMAL" parameter
	to build_special_member_call function.

2011-10-06  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cilk.c (cp_make_cilk_frame): Added cilk_enter_begin and
	cilk_enter_end function calls into spawn expansion.

2011-09-08  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* cp-tree.h (FOR_SCOPE): Changed FOR_STMT_CHECK to
	FOR_STMT_CHECK2.
	* cilk.c (check_incr): Added a check for variable entity name
	match, not just var.  Removed the assert to check if 1st
	operand is the variable.
	(cp_extract_for_fields): Likewise.

2011-09-07  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* parser.c (cp_parser_jump_statement): Removed "IN_CILK_FOR | " from
	  if (parser->in_statement & (IN_CILK_FOR | IN_CILK_SPAWN)).

2011-08-08  Balaji V. Iyer  <balaji.v.iyer@intel.com>

	* call.c: Added #include "cilk.h"
	(build_over_call, build_call_n, build_call_a,
	build_new_function_call, build_op_call_1, build_op_call,
	build_over_call, build_cxx_call, build_special_member_call,
	build_new_method_call_1, build_new_method_call): Added a new parameter
	called "enum call_context" to pass in whether it is a spawned function
	or not.
	* cilk.c: Added new file.
	* cp-gimpify.c (cp_gimplify_expr): Added CILK_FOR_STMT, and
	SPAWN_STMT  case statements.
	* cp-tree.def: Added "CILK_FOR_STMT"
	(lang_name_cilk): New define
	(CILK_FOR_VAR, CILK_FOR_INIT, CILK_FOR_GRAIN): Likewise.
	(FOR_INIT_STMT, FOR_COND, FOR_EXPR,FOR_BODY, FOR_SCOPE): Likewise.
	(SF_CILK_NESTED): Likewise.
	(BUILT_IN_CILK_SYNC, BUILT_IN_CILK_FRAME): Likewise.
	(BUILT_IN_CILK_DISABLE, BUILT_IN_CILK_ENABLE): Likewise.
	(BUILT_IN_CILK_CLEAN, BUILD_IN_CILK_WRITE): Likewise.
	(BUILT_IN_CILK_ACQUIRE, BUILT_IN_CILK_RELEASE): Likewise.
	(BUILT_IN_CILK_WORKER): Likewise
	(enum call_context): New enum to say the context of the
	function call.
	* decl.c (pop_label): Added a if statement to check if  flag enable
	 cilk is turned on. If so, then we exit.
	(is_cilk_function_decl): Added new function.
	(duplicate_decls): Added new if statement to see if
	flag_enable_cilk is set and if it is a is_cilk_function_decl. If so
	then we return newdecl.
	(initialize_predefined_identifiers): Added "Cilk+" element.
	(finish_function_body): Check if cfun->calls_spawns is set.
	if so, then call cp_make_cilk_frame.
	* name-lookup.c (typedef enum scope_kind): Added "sk_cilk_for" and
	"sk_cilk_block" type.
	* parser.c (cp_parser_cilk_for): Added new function.
	(cp_parser_cilk_for_init_statement): Likewise.
	(cp_parser_cilk_grainsize): Likewise.
	(cp_parser_cilk_for_condition): Likewise.
	(cp_parser_cilk_for_expression_iterator): Likewise.
	(cp_parser_simd_private): Likewise.
	(cp_parser_simd_reduction): Likewise.
	(cp_parser_simd_linear): Likewise.
	(cp_parser_simd_assert): Likewise.
	(cp_parser_simd_vectorlength): Likewise.
	(cp_parser_postfix_expression): Added new case statements
	"RID_CILK_SPAWN" to handle cilk_spawn
	(cp_parser_statement): Added new case statement
	"RID_CILK_FOR," "RID_CILK_SYNC."
	(cp_parser_jump_statement): Added new case statement.
	IN_CILK_SPAWN and IN_CILK_FOR in multiple places.
	(cp_parser_pragma): Added the following case statements:
	PRAGMA_SIMD_ASSERT, PRAGMA_SIMD_NOASSERT, PRAGMA_SIMD_VECTORLENGTH,
	PRAGMA_SIMD_PRIVATE, PRAGMA_SIMD_LINEAR, PRAGMA_SIMD_REDUCTION.
	* pt.c (lookup_template_class_1): Check if the record is cilk or
	if the template arguments are of type cilk. If so, set the appropriate
	defines.
	(tsubst_expr): Added "CILK_FOR_STMT" case statement.
	* semantics.c (finish_cilk_for_cond): Added new function.
	(finish_cilk_for_init_stmt): Likewise.
	(begin_cilk_block): Likewise.
	(finish_cilk_block): Likewise.
	(finish_sync_stmt): Likewise.
	(begin_cilk_for_stmt): Likewise.
	(finish_cilk_for_stmt): Likewise.
	(finish_cilk_for_init_stmt): Likewise.
	(finish_cilk_for_cond): Likewise.
	* Make-lang.in: Added cilk files and functions so that we can compile
	them with the gcc files.
	* tree.c (struct attribute_spec cxx_attribute table): Added "cilk"
	entry.
	(handle_cilk_linkage_attribute): New function.
	* typeck.c (cp_build_function_call, cp_build_function_call_nary):
	Added a new parameter called "enum call_context" to pass in whether
	if is a spawned function or not.
